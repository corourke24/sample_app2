require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
    test "full title helper" do
        assert_equal full_title,         "Caturday Surprise!"
        assert_equal full_title("Help"), "Help | Caturday Surprise!"
    end
    
end