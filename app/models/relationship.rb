class Relationship < ApplicationRecord
    belongs_to :follower, class_name: "User"
    belongs_to :followed, class_name: "User"
    # Rails 5 does not require validations to be explicitly stated, these are included for completeness
    validates :follower_id, presence: true
    validates :followed_id, presence: true
end
