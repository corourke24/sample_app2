class Micropost < ApplicationRecord
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates  :user_id, presence: true
  validates  :content, presence: true, length: { maximum: 140 }#,
    # For production, didn't want to update all test fixtures to include the word cat
    #inclusion: { within: %w(cat).flat_map { |s| [s, s.pluralize] }, message: "must be the word cat/cats" }
  validate :picture_size
  #validate :cat_content   
  
  
  private
    
    # Validates the presence of the word cat in a micropost(doesn't work for some reason)
    def cat_content
      unless @micropost.include?(%w(cat).flat_map { |s| [s, s.pluralize] })
        errors.add(:micropost, "Must contain the word cat")
      end
    end
    
    # Validates the size of an uploaded picture
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "Should be less than 5mb")
      end
    end
end    
    

